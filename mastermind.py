#!/bin/python3

import time
import random
from alive_progress import alive_bar
import subprocess

def compute(j):
    for i in range(j):
        time.sleep(random.randrange(2,20)/100) # process items as usual.
        yield  # insert this :)


def toolbar(i = 10):
    with alive_bar(i) as bar:
       for i in compute(i):
           bar()
    #	return 0

def mastermind():
    # random number within the specified range.
    num = 'hackerman'  
    print("\n \n \n")
    print("Initialisiere Skript...\n")
    toolbar(15)
    toolbar(25)
    toolbar(12)
    time.sleep(0.9)
    print("\n")
    n = str(input("Passwort eingeben:"))
      
    # condition to test equality of the
    # guess made. Program terminates if true.
    if (n == num):
            toolbar()
            time.sleep(0.3)
            print("\n")
            print("Passwort Korrekt!\n")
            print("Datei wird Entschlüsselt:")
            toolbar(35)
            time.sleep(0.5)
    else:
        # ctr variable initialized. It will keep count of 
        # the number of tries the Player takes to guess the number.
        ctr = 0  
      
        # while loop repeats as long as the 
        # Player fails to guess the number correctly.
        while (n != num):
            # variable increments every time the loop
            # is executed, giving an idea of how many
            # guesses were made.
            ctr += 1  
      
            count = 0
      
            # explicit type conversion of an integer to
            # a string in order to ease extraction of digits
            n = str(n)  

            # explicit type conversion of a string to an integer
            num = str(num)  
      
            # correct[] list stores digits which are correct
            correct = ['X']*len(n)
            available = []  	

            for i in range(0, len(n)):
               # checking for equality of digits
               if (i >= len(num)-1):
                  continue
               if (n[i] == num[i]):
                  # number of digits guessed correctly increments
                  count += 1
                  # hence, the digit is stored in correct[].
                  correct[i] = n[i]
               elif (n[i] in num):
                  available.append(n[i])
               else:
                  continue

            # when not all the digits are guessed correctly.
            if (count < len(num)): # and (count != 0):
                toolbar()
                time.sleep(1)
                print("\n") 
                print("Entschlüsselungsversuch gescheitert.")
                print("\n")
                time.sleep(1.2)
                if (len(n) < len(num)):
                   print("Getestetes Passwort zu kurz.\n")
                elif (len(n) > len(num)):
                   print("Getestetes Passwort zu lang.\n")
                elif (len(n) == len(num)):
                   print("Passwort-Länge korrekt.\n")
                time.sleep(1)
                if (count != 0):
                   print(count," Zeichen auf der richtigen Position:")
                   time.sleep(0.8)
                   for k in correct:
                       print(k, end =" ")
                       #time.sleep(0.2)

                else:
                   print("Keine Zeichen auf der richtigen Position.")
                time.sleep(1.2)
                if available is not []:
                    available = set(available)
                    print('\n')
                    print("Im Passwort enthaltene Zeichen:")
                    time.sleep(0.7)
                    print( available)

                print('\n')
                print('\n')
                
                time.sleep(1.4)
                n = str(input("Nächsten Versuch eingeben: "))
      
            # when none of the digits are guessed correctly.
    #        elif (count == 0):  
    #            print("None of the numbers in your input match.")
    #            n = str(input("Nächsten Versuch eingeben: "))
      
        # condition for equality.
        toolbar()
        time.sleep(0.3)
        print("\n")
        print("Passwort Korrekt!\n")
        print("Datei wird Entschlüsselt:")
        toolbar(35)
        time.sleep(0.5)
    #exit(0)
    #subprocess.Popen(["evince", "~/Schreibtisch/123098.pdf", "&"])
if __name__ == "__main__":
    mastermind()
