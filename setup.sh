#!/bin/bash

INSTALL_DIR=~/.local/mastermind

mkdir -p $INSTALL_DIR

sudo apt install -y git
sudo apt install -y python3-pip
pip3 install alive-progress

cd ~/Downloads
git clone https://gitlab.com/alexander_schoch/mastermind

cd mastermind

cp *.pdf $INSTALL_DIR
cp script*.sh $INSTALL_DIR
cp *.py $INSTALL_DIR
cp *.desktop ~/Schreibtisch/

chmod +x $INSTALL_DIR/*.sh
chmod +x ~/Schreibtisch/*.desktop

cd
rm -rf Downloads/mastermind
